# Stop Showstopper 🎥🎤

Sometimes native video conferencing software is blocking your input devices (camera / microphone) even after your session has ended. This simple script is just ending those processes so the devices can be used again.
