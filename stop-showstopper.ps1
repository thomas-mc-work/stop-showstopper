# Simple script to stop processes that occasionally block the camera/microphone

Stop-Process -Name "Zoom"
Stop-Process -Name "Teams"
# Cisco Webex
Stop-Process -Name "atmgr"
